use std::{fs::File, io::Read};

use anyhow::anyhow;

fn main() -> anyhow::Result<()> {
    let contents = {
        let mut file = File::open("challenge.txt")?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;
        contents
    };

    let nums = contents
        .lines()
        .map(|num| num.parse())
        .collect::<Result<Vec<usize>, _>>()?;

    match namt::find_collapse(&nums) {
        Ok(Some((idx, num))) => {
            let head = &nums[idx - namt::DEFAULT_QUEUE_SIZE..idx];
            let idx = idx + 1;
            println!("Tunnel would collapse at #{idx}, with {num}; => {head:?}");
            Ok(())
        }
        Ok(None) => Err(anyhow!("Unbreakable tunnel!")),
        Err(err) => Err(err.into()),
    }
}
