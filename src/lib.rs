/*
 * The first 25 numbers of the mine are always secure, but after that, the next number is only safe if it is the sum of 2 numbers in the previous 25
 */

use itertools::Itertools;

pub const DEFAULT_QUEUE_SIZE: usize = 25;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("The tunnel section was too short")]
    SectionTooShort,
}

/// Verify that the given number is not present in the list of sums
///
/// The list of sums is made by summing each pair of numbers in the queue
fn would_collapse_tunnel<const QUEUE_SIZE: usize>(
    queue: &[usize; QUEUE_SIZE],
    needle: usize,
) -> bool {
    queue
        .iter()
        .cartesian_product(queue.iter())
        .map(|(a, b)| a + b)
        .find(|sum| &needle == sum)
        .is_none()
}

/// Attempt to find where this section of inputs would end
///
/// # Return
/// Returns the index in the section that would collapse the tunnel
/// as well as the acutal number
///
/// # Errors
/// Will return [`Error::SectionTooShort`] if the section is less than QUEUE_SIZE + 1
pub fn find_collapse(tunnel: &[usize]) -> Result<Option<(usize, usize)>, Error> {
    if tunnel.len() < DEFAULT_QUEUE_SIZE + 1 {
        return Err(Error::SectionTooShort);
    }

    Ok(tunnel
        .windows(DEFAULT_QUEUE_SIZE + 1)
        .enumerate()
        .find(|(_, section)| {
            let env = arrayref::array_ref!(section, 0, DEFAULT_QUEUE_SIZE);
            let needle = section[DEFAULT_QUEUE_SIZE];

            would_collapse_tunnel(env, needle)
        })
        //offset index by QUEUE_SIZE as we assume the first 25 numbers to be valid
        .map(|(i, section)| (DEFAULT_QUEUE_SIZE + i, section[DEFAULT_QUEUE_SIZE])))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn small_scale_break() {
        let inputs = [
            35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309,
            576,
        ];
        let inputs = arrayref::array_ref!(inputs, inputs.len() - 5, 5);

        let result = would_collapse_tunnel(&inputs, 127);

        assert!(result);
    }

    #[test]
    fn challenge_break() {
        let inputs = [
            67259777, 58569093, 59143180, 60955897, 61452089, 105459988, 82739954, 66093334,
            66505448, 68822480, 69988923, 71327430, 82506061, 102858190, 83090239, 97912385,
            99433653, 99590559, 103234074, 103077168, 122105380, 120021182, 157055565, 118917095,
            117712273, 88311122,
        ];

        let result = find_collapse(&inputs).expect("length is fine");

        assert_eq!(
            Some((DEFAULT_QUEUE_SIZE, inputs[DEFAULT_QUEUE_SIZE])),
            result
        )
    }

    #[test]
    fn challenge_break_core() {
        let inputs = [
            67259777, 58569093, 59143180, 60955897, 61452089, 105459988, 82739954, 66093334,
            66505448, 68822480, 69988923, 71327430, 82506061, 102858190, 83090239, 97912385,
            99433653, 99590559, 103234074, 103077168, 122105380, 120021182, 157055565, 118917095,
            117712273,
        ];

        let result = would_collapse_tunnel(&inputs, 88311122);

        assert!(result)
    }

    #[test]
    fn challenge_no_break_core() {
        let inputs = [
            67259777, 58569093, 59143180, 60955897, 61452089, 105459988, 82739954, 66093334,
            66505448, 68822480, 69988923, 71327430, 82506061, 102858190, 83090239, 97912385,
            99433653, 99590559, 103234074, 103077168, 122105380, 120021182, 157055565, 118917095,
            117712273,
        ];

        let result = would_collapse_tunnel(&inputs, 120099077);

        assert!(!result)
    }
}
